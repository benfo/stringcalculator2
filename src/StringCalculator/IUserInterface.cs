﻿namespace StringCalculator
{
    public interface IUserInterface
    {
        string Read();
        void Print(string value);
    }
}