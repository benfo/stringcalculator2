﻿namespace StringCalculator
{
    public interface IExceptionService
    {
        void NotifyException(string exceptionMessage);
    }
}