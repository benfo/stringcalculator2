﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringCalculator
{
    public class NumberParser : INumberParser
    {
        private static readonly string[] DefaultDelimiters = { ",", "\n" };
        private static readonly Regex InputFormatRegex = new Regex(@"^(?:\/\/(?<delimiters>.+)\n)?(?<numbers>[\s\S]+)$");
        private static readonly Regex DelimiterFormatRegex = new Regex(@"\[(?<delimiter>[^\]]+)\]");

        public int[] Parse(string input)
        {
            var match = InputFormatRegex.Match(input);

            var numbers = GetNumbers(match);
            var delimiters = GetDelimiters(match);

            return numbers
                .Split(delimiters, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();
        }

        private static string GetNumbers(Match match)
        {
            return match.Groups["numbers"].Value;
        }

        private static string[] GetDelimiters(Match match)
        {
            var delimiterGroup = match.Groups["delimiters"];

            if (!delimiterGroup.Success) return DefaultDelimiters;

            var multipleDelimiterMatch = DelimiterFormatRegex.Match(delimiterGroup.Value);

            return !multipleDelimiterMatch.Success
                ? new[] { delimiterGroup.Value }
                : GetMultipleDelimiters(multipleDelimiterMatch);
        }

        private static string[] GetMultipleDelimiters(Match match)
        {
            var delimiters = new List<string>();

            while (true)
            {
                delimiters.Add(match.Groups["delimiter"].Value);

                match = match.NextMatch();
                if (!match.Success)
                    break;
            }

            return delimiters.ToArray();
        }
    }
}