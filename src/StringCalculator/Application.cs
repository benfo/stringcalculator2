﻿using System.Globalization;

namespace StringCalculator
{
    public class Application
    {
        private readonly ICalculator calculator;
        private readonly IUserInterface userInterface;

        public Application(ICalculator calculator, IUserInterface userInterface)
        {
            this.calculator = calculator;
            this.userInterface = userInterface;
        }

        public void Run()
        {
            while (true)
            {
                var input = userInterface.Read();

                if (string.IsNullOrEmpty(input))
                {
                    break;
                }

                var result = calculator.Add(input);
                
                userInterface.Print(result.ToString(CultureInfo.InvariantCulture));
            }
        }
    }
}