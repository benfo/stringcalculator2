﻿using System.Collections.Generic;

namespace StringCalculator
{
    public interface INumberParser
    {
        int[] Parse(string input);
    }
}