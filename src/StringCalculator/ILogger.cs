﻿namespace StringCalculator
{
    public interface ILogger
    {
        void Log(string message);
    }
}
