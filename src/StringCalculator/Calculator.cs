using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class Calculator : ICalculator
    {
        private readonly INumberParser numberParser;
        private readonly ILogger logger;
        private readonly IExceptionService exceptionService;

        public Calculator(INumberParser numberParser, ILogger logger, IExceptionService exceptionService)
        {
            this.numberParser = numberParser;
            this.logger = logger;
            this.exceptionService = exceptionService;
        }

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var numbersToSum = numberParser.Parse(numbers);
            
            ValidateNumbers(numbersToSum);
            var filteredNumbers = FilterNumbers(numbersToSum);
            var sum = SumNumbers(filteredNumbers);

            return sum;
        }

        private int SumNumbers(IEnumerable<int> filteredNumbers)
        {
            var sum = filteredNumbers.Sum();

            try
            {
                logger.Log(sum.ToString());
            }
            catch (Exception ex)
            {
                exceptionService.NotifyException(ex.Message);
            }

            return sum;
        }

        private void ValidateNumbers(IEnumerable<int> numbersToSum)
        {
            var negatives = numbersToSum.Where(number => number < 0);

            if (negatives.Any())
            {
                var message = string.Format("Negatives not allowed: {0}",
                    string.Join(",", negatives));
                throw new NegativeNumbersException(message);
            }
        }

        private IEnumerable<int> FilterNumbers(IEnumerable<int> numbersToSum)
        {
            return numbersToSum.Where(number => number <= 1000);
        }
    }
}