﻿using NUnit.Framework;
using StringCalculator;

namespace StringCalculatorTests
{
    public class NumberParserTests
    {
        private INumberParser numberParser;

        [SetUp]
        public void TestSetUp()
        {
            numberParser = new NumberParser();
        }

        [Test]
        public void Parse_EmptyString_ReturnsEmptyList()
        {
            var result = numberParser.Parse("");

            CollectionAssert.IsEmpty(result);
        }

        [Test]
        [TestCase("1,2", new[] { 1, 2 })]
        [TestCase("1,2,5,8,10", new[] { 1, 2, 5, 8, 10 })]
        public void Parse_NumbersCommaDelimiters_ReturnsListOfNumbers(string numbers, int[] expected)
        {
            var result = numberParser.Parse(numbers);

            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        [TestCase("1\n2", new[] { 1, 2 })]
        [TestCase("1\n2,5,8\n10", new[] { 1, 2, 5, 8, 10 })]
        public void Parse_NumersWithNewLineDelimiters_ReturnsListOfNumbers(string numbers, int[] expected)
        {
            var result = numberParser.Parse(numbers);

            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        [TestCase("//;\n1;2", new[] { 1, 2 })]
        [TestCase("//xx\n1xx2", new[] { 1, 2 })]
        public void Parse_InputWithOneCustomDelimiter_ReturnsListOfNumbers(string numbers, int[] expected)
        {
            var result = numberParser.Parse(numbers);

            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        [TestCase("//[*][%]\n1*2%3", new[] { 1, 2, 3 })]
        [TestCase("//[$][@]\n1@2$3", new[] { 1, 2, 3 })]
        [TestCase("//[$@][**]\n1$@2**3", new[] { 1, 2, 3 })]
        public void Parse_InputWithMultipleDelimiters_ReturnsListOfNumbers(string numbers, int[] expected)
        {
            var result = numberParser.Parse(numbers);

            CollectionAssert.AreEqual(expected, result);
        }
    }
}