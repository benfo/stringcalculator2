﻿using System;
using NSubstitute;
using NUnit.Framework;
using StringCalculator;

namespace StringCalculatorTests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void Add_EmptyString_Returns0()
        {
            var calculator = GetCalculator();
            var result = calculator.Add(string.Empty);

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void Add_OneNumber_ReturnsThatNumber()
        {
            var numberParser = "1".ToNumberParser(1);
            var calculator = GetCalculator(numberParser);

            var result = calculator.Add("1");

            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public void Add_ManyNumbers_ReturnsSum()
        {
            var numberParser = "1,2,3".ToNumberParser(1, 2, 3);
            var calculator = GetCalculator(numberParser);

            var result = calculator.Add("1,2,3");

            Assert.That(result, Is.EqualTo(6));
        }

        [Test]
        public void Add_ContainsOneNegativeNumber_ThrowsNegativeNumbersException()
        {
            var numberParser = "1,-2,2".ToNumberParser(1, -2, 2);
            var calculator = GetCalculator(numberParser);

            var ex = Assert.Throws<NegativeNumbersException>(() => calculator.Add("1,-2,2"));
            Assert.That(ex.Message, Is.EqualTo("Negatives not allowed: -2"));
        }

        [Test]
        public void Add_ContainsMultipleNegativeNumber_ThrowsNegativeNumbersException()
        {
            var numberParser = "1,-2,2,-1,-6".ToNumberParser(1, -2, 2, -1, -6);
            var calculator = GetCalculator(numberParser);

            var ex = Assert.Throws<NegativeNumbersException>(() => calculator.Add("1,-2,2,-1,-6"));
            Assert.That(ex.Message, Is.EqualTo("Negatives not allowed: -2,-1,-6"));
        }

        [Test]
        [TestCase(new[] { 2, 1001 }, 2)]
        [TestCase(new[] { 2, 1001, 10 }, 12)]
        [TestCase(new[] { 1000 }, 1000)]
        public void Add_IgnoresNumbersBiggerThan1000_ReturnsSum(int[] parsedNumbers, int sum)
        {
            var numbersToAdd = string.Join(",", parsedNumbers);
            var numberParser = numbersToAdd.ToNumberParser(parsedNumbers);
            var calculator = GetCalculator(numberParser);

            var result = calculator.Add(numbersToAdd);

            Assert.That(result, Is.EqualTo(sum));
        }

        [Test]
        public void Add_LogIsCalled_WithSumOfNumbers()
        {
            var numberParser = "1,2,3".ToNumberParser(1, 2, 3);
            var logger = Substitute.For<ILogger>();

            var calculator = GetCalculator(numberParser, logger);

            var result = calculator.Add("1,2,3");

            logger.Received().Log(result.ToString());
        }

        [Test]
        public void Add_LoggingThrowsException_ExceptionServiceNotifyExceptionIsCalled()
        {
            var logger = Substitute.For<ILogger>();
            logger
                .When(l => l.Log(Arg.Any<string>()))
                .Do(info =>
                { 
                    throw new Exception("log exception");
                });
            var exceptionService = Substitute.For<IExceptionService>();

            var calculator = GetCalculator(
                logger: logger, 
                exceptionService: exceptionService);

            calculator.Add("1,2,3");

            exceptionService.Received().NotifyException("log exception");
        }

        private static Calculator GetCalculator(INumberParser numberParser = null, ILogger logger = null, IExceptionService exceptionService = null)
        {
            return new Calculator(
                numberParser ?? Substitute.For<INumberParser>(),
                logger ?? Substitute.For<ILogger>(),
                exceptionService ?? Substitute.For<IExceptionService>());
        }
    }
}