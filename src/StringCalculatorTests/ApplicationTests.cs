﻿using NSubstitute;
using NUnit.Framework;
using StringCalculator;

namespace StringCalculatorTests
{
    [TestFixture]
    public class ApplicationTests
    {
        private ICalculator calculator;
        private IUserInterface userInterface;

        [SetUp]
        public void SetUp()
        {
            calculator = Substitute.For<ICalculator>();
            userInterface = Substitute.For<IUserInterface>();
        }

        [Test]
        public void Run_ReadsInputAndCallCalculatorAdd()
        {
            userInterface.Read()
                .Returns("1,2,3", "");

            var app = GetApplication();
            
            app.Run();

            calculator.Received().Add("1,2,3");
        }

        [Test]
        public void Run_ReadsInputUntilBlankInputReceived()
        {
            userInterface.Read()
                .Returns("1,2,3", "4,5,6", "");

            var app = GetApplication();
            app.Run();

            userInterface.Received(3).Read();
        }

        [Test]
        public void Run_PrintsCalculatedResult()
        {
            userInterface.Read()
                .Returns("1,2,3", "");
            calculator.Add(Arg.Any<string>())
                .Returns(6);

            var app = GetApplication();
            app.Run();

            userInterface.Received().Print("6");
        }

        private Application GetApplication()
        {
            return new Application(calculator, userInterface);
        }
    }
}