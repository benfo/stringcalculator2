﻿using NSubstitute;
using StringCalculator;

namespace StringCalculatorTests
{
    public static class HelperExtensions
    {
        public static INumberParser ToNumberParser(this string numbers, params int[] numbersToReturn)
        {
            var numberParser = Substitute.For<INumberParser>();
            numberParser
                .Parse(numbers)
                .Returns(numbersToReturn);
            return numberParser;

        }
    }
}
